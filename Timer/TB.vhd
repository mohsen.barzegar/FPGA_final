--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   20:10:16 12/27/2016
-- Design Name:   
-- Module Name:   E:/Electrical Enginiring/FPGA/TA/FinalProject/Timer/TB.vhd
-- Project Name:  Timer
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Timer
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY TB IS
END TB;
 
ARCHITECTURE behavior OF TB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Timer
    PORT(
         clk : IN  std_logic;
         reset : IN  std_logic;
         control : IN  std_logic_vector(1 downto 0);
         Hs : OUT  std_logic_vector(6 downto 0);
         S : OUT  std_logic_vector(6 downto 0);
         M : OUT  std_logic_vector(6 downto 0);
         H : OUT  std_logic_vector(6 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   signal control : std_logic_vector(1 downto 0) := (others => '0');

 	--Outputs
   signal Hs : std_logic_vector(6 downto 0);
   signal S : std_logic_vector(6 downto 0);
   signal M : std_logic_vector(6 downto 0);
   signal H : std_logic_vector(6 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Timer PORT MAP (
          clk => clk,
          reset => reset,
          control => control,
          Hs => Hs,
          S => S,
          M => M,
          H => H
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      
		reset <= '1' ;
      wait for clk_period;
		
		reset <= '0' ;
		control <= "01" ;
      wait for clk_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
