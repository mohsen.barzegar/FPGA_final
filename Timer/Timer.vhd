----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:54:46 12/27/2016 
-- Design Name: 
-- Module Name:    Timer - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Timer is
port (clk, reset : IN std_logic ;
		control : IN std_logic_vector(1 downto 0) ;
		Hs, S, M, H : OUT std_logic_vector(6 downto 0) 
		);
end Timer;

architecture Arch of Timer is
type states is (Up, Down, Stop, Start);
signal state, next_state : states ;
signal  Hsec, next_Hsec, sec, next_sec, min, next_min, hour, next_hour : unsigned(6 downto 0) ;
--signal  sec, next_sec, min, next_min : integer range 0 to 59 :=0 ;
--signal hour, next_hour : integer range 0 to 23 := 0;
begin

Hs <= std_logic_vector(Hsec);
S <= std_logic_vector(sec);
M <= std_logic_vector(min);
H <= std_logic_vector(Hour);
main : process(clk, reset)
begin
	if (reset = '1') then
		state <= start ;
		Hsec <= (others=>'0') ;
		sec <= (others=>'0') ;
		min <= (others=>'0') ;
		hour <= (others=>'0') ;
	elsif (clk'event and clk = '1') then 
		state <= next_state ;
		Hsec <= next_Hsec ;
		sec <= next_sec ;
		min <= next_min ;
		hour <= next_hour ;
	end if ;
end process ;

stateP : process(control)
begin
	case control is
		when "00" =>
			next_state <= start ;
		when "01" => 
			next_state <= up ;
		when "10" => 
			next_state <= down ;
		when others => 
			next_state <= stop ;
	end case ;
end process;

nexts : process(state, Hsec, sec, min, hour)
begin
	case state is 
		when start =>
			next_Hsec <= (others=>'0') ;
			next_sec <= (others=>'0') ;
			next_min <= (others=>'0') ;
			next_hour <= (others=>'0') ;
		when up =>
			if (Hsec = "1100011") then
				next_Hsec <= (others=>'0') ;
				if(sec = "0111011") then
					next_sec <= (others=>'0') ;
					if(min = "0111011") then
						next_min <= (others=>'0') ;
						next_hour <= hour + 1 ;
					else
						next_min <= min + 1 ;
					end if ;
				else
					next_sec <= sec + 1 ;
				end if ;
			else
				next_Hsec <= Hsec + 1 ;
			end if ;
		when down =>
			if (Hsec = "0000000") then
				next_Hsec <= "1100011" ;
				if(sec = "0000000") then
					next_sec <= "0111011" ;
					if(min = "0000000") then
						next_min <= "0111011" ;
						next_hour <= hour - 1 ;
					else
						next_min <= min - 1 ;
					end if ;
				else
					next_sec <= sec - 1 ;
				end if ;
			else
				next_Hsec <= Hsec - 1 ;
			end if ;
		when others =>
			next_Hsec <= Hsec ;
			next_sec <= sec ;
			next_min <= min ;
			next_hour <= hour ;
	end case ;
end process ;
end Arch;

