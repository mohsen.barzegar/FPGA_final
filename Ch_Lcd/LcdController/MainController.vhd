----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:16:34 12/25/2016 
-- Design Name: 
-- Module Name:    MainController - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MainController is
port ( clk, reset, lcd_enable : IN std_logic ;
		 lcd_bus : IN std_logic_vector(9 downto 0);
--		 busy : OUT std_logic ;
		 rs, rw, e : OUT std_logic;
		 lcd_data : OUT std_logic_vector(7 downto 0)
		 );
		 
end MainController;

architecture Arch1 of MainController is
type LCD_state is (start, power_up, init, rdy, send);
signal state, next_state : LCD_state := start;
signal nclk : std_logic ;
signal clk_count : integer range 0 to 50000 := 0 ;
signal clk_cnt : integer range 0 to 50 :=0 ;
begin

main : process(clk, reset)
begin 
	if (reset = '1') then
		state <= start ;
	elsif (clk'event and clk = '1') then
		state <= next_state ;
	end if;
end process;

clkP : process(clk,reset)
begin
	if (reset = '1') then
		nclk <='0' ;
		clk_cnt <= 0 ;
	elsif (clk'event and clk = '1') then
		if (clk_cnt <= 25) then
			nclk <= '1' ;
			clk_cnt <= clk_cnt + 1 ;
		elsif (clk_cnt = 50) then
			clk_cnt <= 0 ;
		end if;
	end if ;
end process ;
		

states : process(nclk)
begin
	if( nclk'event and nclk = '1') then
		case state is
			when start =>
					next_state <= power_up ;
					clk_count <= 0 ;
			when power_up =>
				if(clk_count < 50000) then
					clk_count <= clk_count + 1 ;
				else
					clk_count <= 0 ;
					rs <= '0' ;
					rw <= '0' ;
					lcd_data <= "00110000";
					next_state <= init ;
				end if ;
			when init =>
				clk_count <= clk_count + 1;
				if(clk_count < 10) then       --function set
					lcd_data <= "00111100";      --2-line mode, display on
					--lcd_data <= "00110100";    --1-line mode, display on
					--lcd_data <= "00110000";    --1-line mdoe, display off
					--lcd_data <= "00111000";    --2-line mode, display off
					e <= '1';
					next_state <= init;
				elsif(clk_count < 60) then   --wait 50 us
					lcd_data <= "00000000";
					e <= '0';
					next_state <= init;
				elsif(clk_count < 70) then      --display on/off control
					lcd_data <= "00001100";      --display on, cursor off, blink off
					--lcd_data <= "00001101";    --display on, cursor off, blink on
					--lcd_data <= "00001110";    --display on, cursor on, blink off
					--lcd_data <= "00001111";    --display on, cursor on, blink on
					--lcd_data <= "00001000";    --display off, cursor off, blink off
					--lcd_data <= "00001001";    --display off, cursor off, blink on
					--lcd_data <= "00001010";    --display off, cursor on, blink off
					--lcd_data <= "00001011";    --display off, cursor on, blink on            
					e <= '1';
					next_state <= init;
				elsif(clk_count < 120 ) then   --wait 50 us
					lcd_data <= "00000000";
					e <= '0';
					next_state <= init;
				elsif(clk_count < 130) then   --display clear
					lcd_data <= "00000001";
					e <= '1';
					next_state <= init;
				elsif(clk_count < 2130) then  --wait 2 ms
					lcd_data <= "00000000";
					e <= '0';
					next_state <= init;
				elsif(clk_count < 2140) then  --entry mode set
					lcd_data <= "00000110";      --increment mode, entire shift off
					--lcd_data <= "00000111";    --increment mode, entire shift on
					--lcd_data <= "00000100";    --decrement mode, entire shift off
					--lcd_data <= "00000101";    --decrement mode, entire shift on
					e <= '1';
					next_state <= init;
				elsif(clk_count < 2200) then  --wait 60 us
					lcd_data <= "00000000";
					e <= '0';
					next_state <= init;
				else                                   --initialization complete
					clk_count <= 0;
					next_state <= rdy;
				end if;    
       
        --wait for the enable signal and then latch in the instruction
        when rdy =>
          if(lcd_enable = '1') then
            rs <= lcd_bus(9);
            rw <= lcd_bus(8);
            lcd_data <= lcd_bus(7 downto 0);
            clk_count <= 0;            
            next_state <= send;
          else
            rs <= '0';
            rw <= '0';
            lcd_data <= "00000000";
            clk_count <= 0;
            next_state <= rdy;
			 end if ;
			--send instruction to lcd        
        when send =>
			  if(clk_count < 50) then  --do not exit for 50us
				  if(clk_count < 1) then      --negative enable
						e <= '0';
				  elsif(clk_count < (14)) then  --positive enable half-cycle
						e <= '1';
				  elsif(clk_count < (27)) then  --negative enable half-cycle
						e <= '0';
				  end if ;
				  clk_count <= clk_count + 1;
				  next_state <= send;
			  else
				 clk_count <= 0;
				 next_state <= rdy;
			  end if ;
		end case ;
	end if ;
end process;
end Arch1;

